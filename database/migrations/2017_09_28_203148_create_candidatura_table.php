<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidaturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nome');
            $table->string('Email');
            $table->string('Telemovel');
            $table->string('Descricao');
            $table->date('Data_Nasc');
            $table->foreign('Curso')->references('id')->on('curso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatura');
    }
}
