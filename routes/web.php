<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/cursos', function () {
    return view('cursos');
});

Route::get('/contactos', function () {
    return view('contactos');
});

Route::get('/descricao', function () {
    return view('descricao');
});




Route::resource('/cursos', 'CursosController');
