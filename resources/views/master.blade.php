<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="/css/stylesheet.css">
    </head>
    <body>
        @include("nav")

        @yield('content')

        @include("footer")
    </body>
</html>