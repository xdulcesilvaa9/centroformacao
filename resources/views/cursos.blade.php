@extends("master")

@section("content")

    <h1>Cursos</h1>
    <h4>Nome do curso</h4>

    <h4>Data de início</h4>

    <h4>Certificações</h4>

    <h4>Conteúdo de formação</h4>

    <h3>Formulário</h3>

    <form id="contact_form" action="" method="POST" enctype="multipart/form-data">
        <div class="row">
            <label for="name">Nome:</label><br />
            <input id="name" class="input" name="name" type="text" size="30" /><br />
        </div>
        <div class="row">
            <label for="email">Email:</label><br />
            <input id="email" class="input" name="email" type="text" size="30" /><br />
        </div>
        <div class="row">
            <label for="telemovel">Telemovel:</label><br />
            <input id="telemovel" class="input" name="telemovel" type="text" size="30" /><br />
        </div>
        <div class="row">
            <label for="datanasc">Data de Nascimento:</label><br />
            <input id="datanasc" class="input" name="datanasc" type="text" size="30" /><br />
        </div>

        <div class="row">
            <label for="descricao">Descricao:</label><br />
            <textarea id="descricao" class="input" name="descricao" rows="7" cols="30"></textarea><br />
        </div>
        <input id="submit_button" type="submit" value="Enviar" />
    </form>
@endsection